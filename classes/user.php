<?
/**
 * query
 */
class User{
	/**
	 * [admin description] 
	 * @param  [type] $login string [description] user login
	 * @return [type] string [description] role this user
	 */
	static function admin($login){
		$db = DB::MySQLiConnect();
		$query = $db->query("SELECT role FROM users WHERE login = '$login'");
		$query->setFetchMode(PDO::FETCH_ASSOC);
		$result = $query->fetch();
		return $result['role'];
	}

	/**
	 * [getIdUsers description] get id user
	 * @param  [type] $login string [description] user login
	 * @return [type] string [description] id this user
	 */
	static function getIdUsers($login){
		$db = DB::MySQLiConnect();
		$sql_id = "SELECT idusers FROM users WHERE login = '$login'";
		$query = $db->query($sql_id) or die('error getIdUser');
		$query->setFetchMode(PDO::FETCH_ASSOC);
		$result = $query->fetch();
		return $result['idusers'];
	}
	/**
	 * [getUser description]
	 * @param  [type] $idusers string [description]
	 * @return [type] array  [description]
	 */
	static function getUser($idusers){
		$id = (int)$idusers;
		$db = DB::MySQLiConnect();
		$sql_u = "SELECT login, email, role FROM users WHERE idusers = '$id'";
		$query = $db->query($sql_u) or die('error getUser');
		$query->setFetchMode(PDO::FETCH_ASSOC);
		$result = $query->fetch();
		return $result;
	}
	/**
	 * [setUser description] update user data
	 * @param [type] $login string    [description]
	 * @param [type] $password string    [description]
	 * @param [type] $email string    [description]
	 * @param [type] $id string    [description]
	 */
	static function setUser($login, $email, $password, $id){
		$db = DB::MySQLiConnect();
		$sql_setuser = "UPDATE users SET login='$login', email='$email', password='$password' WHERE idusers = '$id'";
		$query = $db->query($sql_setuser) or die('error setUser');
		return true;

	}
	/**
	 * [showUsers description] select all users
	 * @return [type] array [description]
	 */
	static function showUsers(){
		$db = DB::MySQLiConnect();
		$sql_u = "SELECT idusers,login,email,role FROM users";
		$query = $db->query($sql_u) or die('error show users');
		return $query;
	}
}
?>