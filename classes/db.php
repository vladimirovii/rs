<?php
interface Connection{
	static function MySQliConnect();
	static function issetTable($value);
}
/**
 * class connect to database and check isset table
 */
class DB implements Connection{
	/**
	 * [MySQliConnect description] connect to database mysql
	 */
	public static function MySQliConnect(){
		try {
			$conn = new PDO("mysql:host=".HOSTDB.";dbname=".DATABASEDB, USERDB, PASSWORDDB);
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}catch(PDOException $e){
    		echo "Connection failed: " . $e->getMessage();  
		}
		return $conn;
	}
	/**
	 * [issetTable description]
	 * @param  [type] string     [description] name table is checked
	 * @return [type] boolean    [description] true if table exist else false
	 */
	public static function issetTable($tableName){
		$con = DB::MySQliConnect();
		$res = $con->query("SELECT * FROM $tableName");
		if(!$res)
			return false;
		return true;
	}
}
?>