<?
class Register{
	/**
	 * [userToRegister description] processing data and call finction ErrorgetEmail
	 * @param  [type] array [description]
	 * @return [type]       [description]
	 */
	public static function userToRegister($arr){
		$login = Simple::ClearData($arr['login'], 's');
		$email = Simple::ClearData($arr['email'], 's');
		$password = Simple::HashPassword($arr['password']);
		if($login == "" && $email == "") 
			return false;
		// check email
		if(!self::ErrorgetEmail($email)){
			die ("User witch this email exec");
		}
		// 
		if(!self::newUsers($login, $email, $password))
			return false;
		return true;
	}
	/**
	 * [userToAuthorised description] processing data and call finction getValidateAuthorized
	 * @param  [type] array [description]
	 * @return [type]       [description]
	 */
	public static function userToAuthorised($arr){
		$login = Simple::ClearData($arr['login'], 's');
		$password = Simple::HashPassword($arr['password']);
		if(!self::getValidateAuthorized($login, $password))
			return false;
		else
			return true;
	}
	/**
	 * [getValidateAuthorized description] authorised user 
	 * @param  [type] string    [description]
	 * @param  [type] string    [description]
	 * @return [type]           [description]
	 */
	private static function getValidateAuthorized($login, $password){
		$db = DB::MySQLiConnect(); 
		$sql_authorized = "SELECT login, password FROM users WHERE login='$login'";
		$query = $db->query($sql_authorized);
		$query->setFetchMode(PDO::FETCH_ASSOC);
		$result = $query->fetch();
		if($result['login'] == $login && $result['password'] == $password){
			// session start
			try {
				if(!Session::newSessionUsersLogin($login))
					throw new Exception('Errors Session');
				else
					return true;
			} catch (Exception $e) {
				return $e->setMessage();
			}
		}else
			return false;
	}
	/**
	 * [newUsers description] rigester new user
	 * @param  [type] string    [description]
	 * @param  [type] string    [description]
	 * @param  [type] string    [description]
	 * @return [type]           [description]
	 */
	private static function newUsers($login, $email, $password){
		$db = DB::MySQLiConnect();
		$sql_reg = "INSERT INTO users (login, email, password, role) VALUES ('$login', '$email', '$password', 1)";
		$query = $db->query($sql_reg);
		if($query){
			//start session
			try {
				if(!Session::newSessionUsersLogin($login))
					throw new Exception('Errors Session');
				else
					return true;
			} catch (Exception $e) {
				return $e->setMessage();
			}
		}else
			return false;
	}
	/**
	 * [ErrorgetEmail description] exec user witch email?
	 * @param [type] string [description]
	 */
	private static function ErrorgetEmail($email){
		$db = DB::MySQLiConnect();
		$sql_email = "SELECT email FROM users WHERE email='$email'";
		$query = $db->query($sql_email);
		$query->setFetchMode(PDO::FETCH_ASSOC);
		$result = $query->fetch();
		if($result['email'] == $email)
			return false;
		else
			return true;
	}
}
?>