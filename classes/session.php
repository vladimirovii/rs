<?
/**
 * Session
 */
class Session{
	/**
	 * [activate description] session start
	 * @return [type] [description]
	 */
	public static function activate(){
		return session_start();
	}
	/**
	 * [newSessionUsersLogin description] check isset session
	 * @param  [type] string   [description]
	 * @return [type] string   [description]
	 */
	public static function newSessionUsersLogin($this_session){
		$_SESSION['login'] = $this_session;
		Online::startSessionOnline(User::getIdUsers($_SESSION['login']), $_SESSION['login']);
		return $_SESSION['login'];
	}
	/**
	 * [authtrue description] check user registered or authorised
	 * @return [type] boolean [description]
	 */
	public static function authtrue(){
		if(isset($_SESSION['login']))
			return true;
		else
			return false;
	}
	/**
	 * [logout description] delete session
	 * @return [type] boolean [description]
	 */
	public static function logout(){
		if($_SESSION['login']){
			$idUser = User::getIdUsers($_SESSION['login']);
			$idSession = Online::getIdGameSession($idUser);
			$idOpponent = Online::getIdOpponent($idSession, $idUser);
			GameEnd::gameExit($idOpponent);
			GameSessions::endGameSession($idSession);
			Online::endSessionOnline($idUser);
			unset($_SESSION['login']);
			return true;
		}else
			return fasle;
	}
}
?>