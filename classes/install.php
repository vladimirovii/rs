<?php 
/**
 *  Install
 */
class Install {
	public $connect;
	/**
	 * [__construct description] connect to DB
	 */
	public function __construct(){
		$this->connect = DB::MySQliConnect();
	}
	/**
	 * [start description] install table and set user Administrator
	 * @param  [type] array  [description] 
	 * @return [type]        [description]
	 */
	public function start($admin){
		try{
			// Setup Database
			if(!$this->tables()) 
				throw new Exception("error created tables");
			// creating admin
			$login = Simple::ClearData($admin[0], 's');
			$email = Simple::ClearData($admin[1], 's');
			$password = $admin[2];
			if(!$this->newAdmin($login, $email, $password)) 
				throw new Exception("error created admin"); 
	
		}catch(Exception $e){
			return $e->getMessage();
		}
	}
	/**
	 * [tables description] insert table to database
	 * @return [type] boolean [description] true if insert tables success
	 */
	public function tables(){
		$sql = $this->connect;
		$sql->query('set names utf8');
		$sql->query("SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0");
		$sql->query("SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0");
		$sql->query("SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES'");
		$sql->query("CREATE SCHEMA IF NOT EXISTS " . DATABASEDB . " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ; USE " . DATABASEDB);

		$users = $sql->query(
		"CREATE TABLE users (
		idusers INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
		login VARCHAR(30) NOT NULL,
		password VARCHAR(150) NOT NULL,
		email VARCHAR(50),
		role INT(1) NOT NULL
		)");

		$users = $sql->query(
		"CREATE TABLE online (
		idonline INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
		userid INT(11) NOT NULL,
		userlogin VARCHAR(30) NOT NULL,
		reply INT(11) NOT NULL,
		session INT(11) NOT NULL,
		move INT(1) NOT NULL
		)");

		$users = $sql->query(
		"CREATE TABLE gamesessions (
		idsession INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
		sessionid INT(11) NOT NULL,
		col1 INT(1) NOT NULL,
		col2 INT(1) NOT NULL,
		col3 INT(1) NOT NULL,
		col4 INT(1) NOT NULL,
		col5 INT(1) NOT NULL,
		col6 INT(1) NOT NULL,
		col7 INT(1) NOT NULL,
		col8 INT(1) NOT NULL,
		col9 INT(1) NOT NULL
		)");
		
		$sql->query("SET SQL_MODE=@OLD_SQL_MODE");
		$sql->query("SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS");
		$sql->query("SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS");
		
		if(!$users) return false;

		return true;
	}
	/**
	 * [newAdmin description]
	 * @param  [type] string    [description]
	 * @param  [type] string    [description]
	 * @param  [type] string    [description]
	 * @return [type]           [description] true if insert administrator success
	 */
	private function newAdmin($login, $email, $password){
		$sql = $this->connect;
		$insert_admin = "INSERT INTO users (login, email, password, role) VALUES ('$login', '$email', '$password', '3')";
		$result = $sql->query($insert_admin);
		if(!$result) return false;
		return true;
	}
}
?>
