<?php
class Online{

	static function startSessionOnline($userid, $userlogin){
		$db = DB::MySQliConnect();
		$sql = "INSERT INTO online (userid, userlogin) VALUES ('$userid', '$userlogin')";
		$result = $db->query($sql);
		if(!$result) return false;
		return true;
	}
	static function endSessionOnline($userid){
		$db = DB::MySQliConnect();
		$sql = "DELETE FROM online WHERE userid='$userid'";
		$result = $db->query($sql);
		if(!$result) return false;
		return true;
	}
	static function selectOnline(){
		$db = DB::MySQliConnect();
		$query = $db->query("SELECT userid, userlogin FROM online");
		return $query;
	}
	static function requestOnline($id){
		$db = DB::MySQLiConnect();
		$sql = "UPDATE online SET session='$id', move=1 WHERE userid = '$id'";
		$query = $db->query($sql) or die('error request');
		return true;
	}
	static function replyOnline($id, $idhost){
		$db = DB::MySQLiConnect();
		$sql = "UPDATE online SET reply='$idhost', session='$idhost' WHERE userid = '$id'";
		$query = $db->query($sql) or die('error reply');
		return true;	
	}
	static function getReplyOnline($id){
		$db = DB::MySQLiConnect();
		$sql = "SELECT reply FROM online WHERE userid = '$id'";
		$query = $db->query($sql) or die('error reply');
		$query->setFetchMode(PDO::FETCH_ASSOC);
		$result = $query->fetch();
		return $result['reply'];	
	}
	static function replyOkOnline($id){
		$db = DB::MySQLiConnect();
		$sql = "UPDATE online SET reply=0 WHERE userid = '$id'";
		$result = $db->query($sql) or die('error reply ok');
		return true;	
	}
	static function replyCancelOnline($id){
		$db = DB::MySQLiConnect();
		$sql = "UPDATE online SET reply=0, session=0 WHERE userid = '$id'";
		$result = $db->query($sql) or die('error reply cancel');
		return true;	
	}
	static function setMoveOnline($id){
		$db = DB::MySQLiConnect();
		$sql = "UPDATE online SET move=1 WHERE userid = '$id'";
		$query = $db->query($sql) or die('error add move');
		return true;
	}
	static function getMoveOnline($id){
		$db = DB::MySQLiConnect();
		$sql = "SELECT move FROM online WHERE userid = '$id'";
		$query = $db->query($sql) or die('error get move');
		$query->setFetchMode(PDO::FETCH_ASSOC);
		$result = $query->fetch();
		return $result['move'];
	}
	static function removeMoveOnline($id){
		$db = DB::MySQLiConnect();
		$sql = "UPDATE online SET move=0 WHERE userid = '$id'";
		$query = $db->query($sql) or die('error remove move');
		return true;
	}
	static function preGameSession(){
		$db = DB::MySQLiConnect();
		$sql = "SELECT session FROM online GROUP BY session HAVING COUNT(*)>1";
		$query = $db->query($sql) or die('error remove move');
		if(!$query)
			return false;
		return true;
	}
	static function getIdGameSession($id){
		$db = DB::MySQLiConnect();
		$sql = "SELECT session FROM online WHERE userid='$id'";
		$query = $db->query($sql) or die('error getid session');
		$query->setFetchMode(PDO::FETCH_ASSOC);
		$result = $query->fetch();
		return $result['session'];
	}
	static function getIdOpponent($idsession, $idmy){
		$db = DB::MySQLiConnect();
		$sql = "SELECT userid FROM online WHERE SESSION ='$idsession' AND userid !='$idmy'";
		$query = $db->query($sql) or die('error getid opponent');
		$query->setFetchMode(PDO::FETCH_ASSOC);
		$result = $query->fetch();
		return $result['userid'];
	}
}
//SELECT session FROM online WHERE session IN (SELECT session FROM online GROUP BY session HAVING COUNT(*)>1)
?>