<?php if(!$_GET): ?>
<div class="step">
	Step
	<?php echo $_GET = $_GET?$_GET:1; ?>/3</div>
<div class="content">
	<div class="text">
		<form name="copy" action="?page=2" method="post">
			<h3>License agreement</h3>
			<textarea name="copy" rows="5" cols="65" wrap="off" disabled="on">
				<?php echo Simple::openTextFiles(DOMAIN.'/engine/install/install.dat'); ?></textarea>
			<p> <strong>Agree:</strong>
			</p>
			<input type="checkbox" name="yes" value="ok">
			<input type="submit" value="Next" class="button" name="system"></form>
	</div>
</div>
<?php elseif ($_GET['page'] == 2): ?>
<div class="step">
	Step
	<?php echo $_GET['page'] = $_GET['page']?$_GET['page']:2; ?>/3</div>
<div class="content">
	<div class="text">
		<h3>Setup database</h3>
		<p>Enter data MySQL server</p>
		<form action="?page=3" method="post">
			<fieldset>
				<legend>Database name</legend>
				<input name="dbname" type="text" value="" placeholder="Name Database"> 
				<i>If DB is not created, do it now.</i> 
			</fieldset>
			<fieldset>
				<legend>User name MySQL</legend>
				<input name="dbuser" type="text" value="" placeholder="User name"> 
				<i>User name MySQL server (default: root)</i>
			</fieldset>
			<fieldset>
				<legend>User password MySQL</legend>
				<input name="dbpassword" type="text" value="" placeholder="User password">
				<i>User password MySQL server</i>
			</fieldset>
			<fieldset>
				<legend>Server name MySQL</legend>
				<input name="dbserver" type="text" value="" placeholder="Server name">
				<i>Name MySQL server (default: localhost)</i>
			</fieldset>
			<input type="submit" class="button" name="second" value="Next"></form>
	</div>
</div>
<?php elseif ($_GET['page'] == 3): ?>
<div class="step">
	Step
	<?php echo $_GET['page'] = $_GET['page']?$_GET['page']:3; ?>/3</div>
<div class="content">
	<div class="text">
		<form action="?page=3" method="post">
			<fieldset>
				<legend>E-mail administrator</legend>
				<input name="adminemail" type="text" value="" placeholder="E-mail administrator"> 
			</fieldset>
			<fieldset>
				<legend>Login administrator</legend>
				<input name="adminlogin" type="text" value="" placeholder="Administrator login">
			</fieldset>
			<fieldset>
				<legend>Password administrator</legend>
				<input name="adminpassword" type="text" value="" placeholder="Administrator password">
			</fieldset>
			<input type="submit" class="button" name="third" value="Finish">
		</form>
	</div>
</div>
<?php endif; ?>