<h3>Profile</h3>
<?
$idusers = User::getIdUsers($_SESSION['login']);
$profile = User::getUser($idusers);

$login = trim($profile['login']);
$email = trim($profile['email']);
$role = trim($profile['role']);

$role_id = Simple::getRole($role);

if($_POST){
	$login = Simple::ClearData($_POST['login'], 's');
	$email = Simple::ClearData($_POST['email'], 's');
	$password = Simple::HashPassword($_POST['password']);
	if($login == "" || $email == "") 
		die("Error enter login, email");
	try {
		if(!User::setUser($login, $email, $password, $idusers))
			throw new Exception("Error save");
		else{
			$_SESSION['login'] = $login;
			echo "Saved accept";
		}
	} catch (Exception $e) {
		die($e->getMessage());
	}
}
?>
<table>
	<tr><td>Login: </td><td><?=$login;?></td></tr>
	<tr><td>Email: </td><td><?=$email;?></td></tr>
	<tr><td>Group: </td><td><?=$role_id;?></td></tr>
</table>
<button type="button" class="button" id="myBtn">Change profile</button>
<div id="myfrm" style="display: none">
	<center>
		<h4>Change user profile</h4>
		<form method="post">
			<table>
				<tr><td>Login: </td><td><input type="text" name="login" value="<?= $login; ?>"></td></tr>
				<tr><td>Email: </td><td><input type="text" name="email" value="<?= $email; ?>"></td></tr>
				<tr><td>Password: </td><td><input type="text" name="password"></td></tr>
				<tr><td colspan="2"><input type="submit" class="button" value="save" onclick="validate(this.form)"></td></tr>
			</table>
		</form>
	</center>
</div>

<script type="text/javascript">
$(document).ready(function(){
    $('#myBtn').click(function(){
        $('#myfrm').fadeTo("slow", 1);
    });
});
</script>