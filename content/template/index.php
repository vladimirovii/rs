<?	
	// include header
	include_once 'header.php';
	
	try {
		if($_GET){
			$files = array(
				"register" => "register",
			);
			foreach ($_GET as $menu => $value) {
				if($menu != $files[$menu])
					return print "Page not found";
				if($menu == 'register')
					include_once TPL_DIR . $menu . '.php'; 
			}
		}else{
			include_once 'home.php';
		}
		// if new user registered
		if(isset($_POST['register'])){
			if(!Register::userToRegister($_POST)) throw new Exception('Error registration');
			else{
				header ('Refresh: 0.5; url=index.php');
				echo 'Success registration';
				exit();
			}
		}
		// if user authorized
		if(isset($_POST['authorized'])){
			if(!Register::userToAuthorised($_POST)) throw new Exception("User not found");
			else{
				header ('Refresh: 0.5; url=index.php');
				echo 'Success enter';
				exit();
			}
		}
		//footer
		include_once 'footer.php';
	}catch (Exception $e) {
		die($e->getMessage());
		}

?>