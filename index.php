<?
	// include file config
	require_once 'config.php';

	//if not file config.db to install 
	if(!file_exists(INCLUDES_DIR . 'config.db.php')){
		header('Location: install.php');
	}else{
	//check install database
	require_once INCLUDES_DIR . 'config.db.php';
	if(!DB::issetTable('users')) die('DB not found, delete file config.db and setup DB!');
	//else check autorized 
	if(!Session::authtrue())
		include_once TPL_DIR . 'index.php';
	else
		include_once TPL_DIR . 'users/index.php';
	}
?>